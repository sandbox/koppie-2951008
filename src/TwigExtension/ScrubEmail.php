<?php
/**
 * Created by PhpStorm.
 * User: koppie
 * Date: 11/9/17
 * Time: 4:07 PM
 *
 * Class DefaultService.
 *
 * @package Drupal\email_scrubber
 */

namespace Drupal\email_scrubber\TwigExtension;

class ScrubEmail extends \Twig_Extension {

  /**
   * {@inheritdoc}
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'email_scrubber.twig_extension';
  }

  /**
   * Generates a list of all Twig filters that this extension defines.
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('scrub_email', array($this, 'ScrubEmail'), array('is_safe' => array('html'))),
    ];
  }

  /**
   * Filter to return sanitized email
   */
  public static function ScrubEmail($email) {
    // If it's a field, then we're getting a keyed array
    $first_key = key($email);
    // Get the first value of the field
    $value = $email[$first_key][0]['#context']['value'];
    // Use centralized function to scrub & return
    return email_scrubber_obfuscate($value);
  }
}